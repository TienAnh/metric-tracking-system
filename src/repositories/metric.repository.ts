import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MetricDataSource} from '../datasources';
import {MetricRelations, Metrics} from '../models';

export class MetricRepository extends DefaultCrudRepository<
  Metrics,
  typeof Metrics.prototype.id,
  MetricRelations
> {
  constructor(
    @inject('datasources.metric') dataSource: MetricDataSource,
  ) {
    super(Metrics, dataSource);
  }
}
