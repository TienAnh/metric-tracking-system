import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, put, requestBody,
  response
} from '@loopback/rest';
import {TIME_CHART, TIME_PERIOD} from '../constants/metric-type';
import {Metrics} from '../models';
import {MetricRepository} from '../repositories';

export class MetricController {
  constructor(
    @repository(MetricRepository)
    public metricRepository: MetricRepository,
  ) { }

  @post('/metrics')
  @response(200, {
    description: 'Metric model instance',
    content: {'application/json': {schema: getModelSchemaRef(Metrics)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Metrics, {
            title: 'NewMetric',
            exclude: ['id'],
          }),
        },
      },
    })
    metric: Omit<Metrics, 'id'>,
  ): Promise<Metrics> {
    return this.metricRepository.create(metric);
  }

  @get('/metrics/count')
  @response(200, {
    description: 'Metric model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Metrics) where?: Where<Metrics>,
  ): Promise<Count> {
    return this.metricRepository.count(where);
  }

  @get('/metrics')
  @response(200, {
    description: 'Array of Metric model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Metrics, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Metrics) filter?: Filter<Metrics>,
  ): Promise<Metrics[]> {
    return this.metricRepository.find(filter);
  }

  @patch('/metrics')
  @response(200, {
    description: 'Metric PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Metrics, {partial: true}),
        },
      },
    })
    metric: Metrics,
    @param.where(Metrics) where?: Where<Metrics>,
  ): Promise<Count> {
    return this.metricRepository.updateAll(metric, where);
  }

  @get('/metrics/{id}')
  @response(200, {
    description: 'Metric model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Metrics, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Metrics, {exclude: 'where'}) filter?: FilterExcludingWhere<Metrics>
  ): Promise<Metrics> {
    return this.metricRepository.findById(id, filter);
  }

  @patch('/metrics/{id}')
  @response(204, {
    description: 'Metric PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Metrics, {partial: true}),
        },
      },
    })
    metric: Metrics,
  ): Promise<void> {
    await this.metricRepository.updateById(id, metric);
  }

  @put('/metrics/{id}')
  @response(204, {
    description: 'Metric PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() metric: Metrics,
  ): Promise<void> {
    await this.metricRepository.replaceById(id, metric);
  }

  @del('/metrics/{id}')
  @response(204, {
    description: 'Metric DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.metricRepository.deleteById(id);
  }

  @get('/metrics-chart')
  @response(200, {
    description: 'Array of Metric model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Metrics, {includeRelations: true}),
        },
      },
    },
  })
  async getChart(
    @param.query.string('type') type: string,
    @param.query.string('timePeriod') timePeriod: string,
    @param.query.string('userId') userId: string,
  ): Promise<Metrics[]> {
    const filter: any = {};
    filter.type = type;
    if (userId) {
      filter.userId = userId;
    }

    switch (timePeriod) {
      case TIME_PERIOD.DAY:
        filter.createdAt = {gte: Date.now() - TIME_CHART.DAY}
        break;
      case TIME_PERIOD.ONE_MONTH:
        filter.createdAt = {gte: Date.now() - TIME_CHART.ONE_MONTH}
        break;
      case TIME_PERIOD.TWO_MONTH:
        filter.createdAt = {gte: Date.now() - TIME_CHART.TWO_MONTH}
        break;
    }
    return this.metricRepository.find({
      where: filter
    });
  }
}
