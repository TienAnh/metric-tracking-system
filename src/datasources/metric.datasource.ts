import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'metric',
  connector: 'mysql',
  url: '',
  host: process.env.DB_HOST || '127.0.0.1',
  port: process.env.DB_PORT || 3306,
  user: process.env.DB_USERNAME || 'root',
  password: process.env.DB_PASSWORD || 'Root1234',
  database: process.env.DB_SCHEMA || 'db_metric',
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MetricDataSource
  extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'metric';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.metric', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
