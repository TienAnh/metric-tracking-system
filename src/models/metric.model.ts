import {Entity, model, property} from '@loopback/repository';
import {METRIC_TYPE, UNIT_TYPE} from '../constants/metric-type';

@model()
export class Metrics extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  value?: number;

  @property({
    type: 'string',
    jsonSchema: {
      enum: METRIC_TYPE
    },
  })
  type?: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: UNIT_TYPE
    },
  })
  unit?: string;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @property({
    type: 'date',
  })
  deletedAt?: string;

  @property({
    type: 'number',
  })
  userId?: number;

  constructor(data?: Partial<Metrics>) {
    super(data);
  }
}

export interface MetricRelations {
  // describe navigational properties here
}

export type MetricWithRelations = Metrics & MetricRelations;
