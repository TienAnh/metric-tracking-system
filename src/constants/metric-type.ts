const METRIC_TYPE = [
  'DISTANCE',
  'TEMPERATURE'
];

const UNIT_TYPE = [
  'METER',
  'CENTIMETER',
  'INCH',
  'FEET',
  'YARD',
  '°C',
  '°F',
  '°K'
];

const TIME_PERIOD = {
  DAY: 'DAY',
  ONE_MONTH: 'ONEMONTH',
  TWO_MONTH: 'TWOMONTH'
}

const TIME_CHART = {
  DAY: 24 * 60 * 60 * 1000,
  ONE_MONTH: 30 * 24 * 60 * 60 * 1000,
  TWO_MONTH: 2 * 30 * 24 * 60 * 60 * 1000
}

export {METRIC_TYPE, UNIT_TYPE, TIME_PERIOD, TIME_CHART};
